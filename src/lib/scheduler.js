// import { Router } from 'express';
import { CronJob } from 'cron'
import AM2320 from 'am2320'
import DHT11 from 'node-dht-sensor'

import subBeehiveData from './jobs/subBeehiveData'
import valuesIn from './jobs/valuesIn'
import valuesOut from './jobs/valuesOut'
import { handleOpen } from './push'

export default ({ config, db, bt }) => {

  const sensorIn = new AM2320(0x5c, '/dev/i2c-1');
  const sensorOut = DHT11;
  const mainBeehiveID = "m6X3Dt6ZBA96FdM3l4IO";
  const subBeehiveID = "CyTbK2gJGQLOb4P0TPbM";
  var toBeJson = "";

  bt.on('data', function(buffer) {
    let strBuff = buffer.toString()
    toBeJson = toBeJson.concat(strBuff);

    if(strBuff.indexOf("E") > -1){ // End of arduino data
      let validJson = splitData(toBeJson);
      pushData(validJson, subBeehiveID, db)
      toBeJson = "";
    }else if(strBuff.indexOf("X") > -1){ // triger opened adruino
      handleOpen(subBeehiveID);
    }
  });

  new CronJob('1 * * * * *', () => sendRequest(bt, subBeehiveID), null, true);

  setTimeout(() => {
    new CronJob('1 * * * * *', () => valuesIn(mainBeehiveID, sensorIn, db), null, true);
    new CronJob('1 * * * * *', () => valuesOut(mainBeehiveID, sensorOut, db), null, true);
  }, 2000)

}

const sendRequest = (bt, subBeehiveID) => {
  // console.log(config.btCodes[code]);
  bt.write(new Buffer('2', 'utf-8'), function(err, bytesWritten) {
    if (err) console.log(err);
    else console.log('Data Request sent to Bluetooth device');
  });
}

const splitData = (data) => JSON.parse(data.substr(data.indexOf("S") + 1, data.indexOf("E") - 1));


const pushData = (data, ID, db) => {
  for(let property in data){
    if(data.hasOwnProperty(property)){
      // console.log(data[property]);
      if(property == "humIn") {

        db.collection('Ule').doc(ID).collection('Humidity').add({
          timestamp: new Date(),
          value: data[property],
          out: false
        })
        .then(snapshot => {
          console.log(`Document for ${ID} - INDOOR added with id: ${snapshot.id}, humidity: ${data[property]}`);
        })
        .catch(err => {
          console.log(`Error adding document - humidity ${err} for ${ID} - INDOOR`);
        });

      }else if(property == "humOut"){

        db.collection('Ule').doc(ID).collection('Humidity').add({
          timestamp: new Date(),
          value: data[property],
          out: true
        })
        .then(snapshot => {
          console.log(`Document for ${ID} - OUTDOOR added with id: ${snapshot.id}, humidity: ${data[property]}`);
        })
        .catch(err => {
          console.log(`Error adding document - humidity ${err} for ${ID} - OUTDOOR`);
        });
      }else if(property == "tempIn"){

        db.collection('Ule').doc(ID).collection('Temperature').add({
          timestamp: new Date(),
          value: data[property],
          out: false
        })
        .then(snapshot => {
          console.log(`Document for ${ID} - INDOOR added with id: ${snapshot.id}, temperature: ${data[property]}`);
        })
        .catch(err => {
          console.log(`Error adding document - temperature ${err} for ${ID} - INDOOR`);
        });
      }else if(property == "tempOut"){

        db.collection('Ule').doc(ID).collection('Temperature').add({
          timestamp: new Date(),
          value: data[property],
          out: true
        })
        .then(snapshot => {
          console.log(`Document for ${ID} - OUTDOOR added with id: ${snapshot.id}, temperature: ${data[property]}`);
        })
        .catch(err => {
          console.log(`Error adding document - temperature ${err} for ${ID} - OUTDOOR`);
        });
      }else{
        console.log(' ');
      }

    }
  }
}
