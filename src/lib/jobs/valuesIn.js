export default (beehiveID, sensor, db) => {

  sensor.readValues()
  .then((values) => {
    // console.log("hum: "+values.humidity);
    // console.log("temp: "+values.temperature);

    db.collection('Ule').doc(beehiveID).collection('Humidity').add({
      timestamp: new Date(),
      value: values.temperature,
      out: false
    })
    .then(snapshot => {
      console.log(`Document for ${beehiveID} - OUTDOOR added with id: ${snapshot.id}, temperature: ${values.temperature}`);
    })
    .catch(err => {
      console.log(`Error adding document - humidity ${err} for ${beehiveID} - OUTDOOR`);
    });


    db.collection('Ule').doc(beehiveID).collection('Temperature').add({
      timestamp: new Date(),
      value: values.humidity,
      out: false
    })
    .then(snapshot => {
      console.log(`Document for ${beehiveID} - OUTDOOR added with id: ${snapshot.id}, humidity: ${values.humidity}`);
    })
    .catch(err => {
      console.log(`Error adding document - humidity ${err} for ${beehiveID} - OUTDOOR`);
    });
  })
  .catch(err => {
    console.log(`Error reading Value IN ${err}`);
  })
}
