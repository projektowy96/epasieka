export default (beehiveID, sensor, db) => {
  sensor.read(11, 17, (err, temperature, humidity) => {
      // if (!err) {
      temperature = 22.2;
      humidity = 22.2;

          db.collection('Ule').doc(beehiveID).collection('Humidity').add({
            timestamp: new Date(),
            value: temperature,
            out: true
          })
          .then(snapshot => {
            console.log(`Document for ${beehiveID} - INDOOR added with id: ${snapshot.id}, temperature: ${temperature}`);
          })
          .catch(err => {
            console.log(`Error adding document - humidity ${err} for ${beehiveID} - INDOOR`);
          });


          db.collection('Ule').doc(beehiveID).collection('Temperature').add({
            timestamp: new Date(),
            value: humidity,
            out: true
          })
          .then(snapshot => {
            console.log(`Document for ${beehiveID} - INDOOR added with id: ${snapshot.id}, humidity: ${humidity} `);
          })
          .catch(err => {
            console.log(`Error adding document - humidity ${err} for ${beehiveID} - INDOOR`);
          });
      //
      // } else {
      //   console.log(`Error reading Value OUT ${err}`);
      // }
  })
}
