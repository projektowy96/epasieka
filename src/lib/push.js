const Gpio = require('onoff').Gpio;
import admin from 'firebase-admin';

export const watchButton = ({ config, db }) => {
  let topic = 'm6X3Dt6ZBA96FdM3l4IO';
  let pushButton = new Gpio(26, 'in', 'both');
  pushButton.watch(function (err, value) {
    if (err) {
      console.error('There was an error', err);
    return;
    }
    // console.log("Button value " + value);

    if(!value){
      handleOpen(topic);
    }
  })
}

export const handleOpen = (topic) => {
  var message = {
    data: {
      msg: 'Beehive with opened!',
      time: `${new Date()}`
    },
    topic: topic
  };

  admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log('Beehive ID: ' + topic + ' open notification');
      // console.log(response);
    })
    .catch((error) => {
      console.log('Error sending message:', error);
    });
  }
