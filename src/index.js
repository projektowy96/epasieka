import http from 'http';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import initializeBt from './middleware/bluetooth';
import api from './api';
import config from './config.json';
import scheduler from './lib/scheduler';
import { watchButton } from './lib/push';

let app = express();
app.server = http.createServer(app);

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json());

// connect to db
initializeDb( db => {

	// internal middleware
	app.use(middleware({ config, db }));

	// api router
	app.use('/api', api({ config, db }));
	initializeBt( bt => {
		app.server.listen(process.env.PORT || config.port, () => {
			console.log(`Started on port ${app.server.address().port}`);
			scheduler({ config, db, bt })
			watchButton({ config, db })
		});
	})
});

export default app;
