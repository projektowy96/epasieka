import { version } from '../../package.json';
import facets from './facets';
import { Router } from 'express';
export default ({ config, db }) => {
	const api = Router();

	// api.use(middleware({ config, db }));

	// mount the facets resource
	api.use('/facets', facets({ config, db }));
	// perhaps expose some API metadata at the root
	api.post('/', (req, res) => {
    const payload = req.body;

		if(config.Ule.indexOf(payload.id) >= 0){
			db.collection('Ule').doc(payload.id).collection('Temperatura').add({
	      timestamp: new Date(parseInt(payload.timestamp)),
	      value: payload.value
	    }).then(snapshot => {
	        console.log(`Document added with id: ${snapshot.id}`);
	        res.send(`Document added with id: ${snapshot.id}`);
	      })
	      .catch(err => {
	        console.log('Error getting document', err);
					res.send(`Fail to save`, err);
	      });
		}else {
			res.send(`Wrong id: ${payload.id}`);
		}
	});

	api.get('/temp', (req, res) => {
		res.send("temp: " + x);
	});

	api.get('/', (req, res) => {
		// console.log(config);
		res.send("test enpoint")
	});

	return api;
}
