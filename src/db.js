import admin from 'firebase-admin';
import functions from 'firebase-functions';
import config from './config';

export default callback => {
	// connect to a database if needed, then pass it to `callback`:

	admin.initializeApp({
	  credential: admin.credential.cert(config.firebaseConfig),
	  databaseURL: config.databaseURL
	});
	const db = admin.firestore();
	admin.firestore().settings( { timestampsInSnapshots: true })

	callback(db);
}
